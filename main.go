package main

import (
	"context"
	"github.com/gorilla/websocket"
	"github.com/nats-io/nats.go"
	"github.com/rs/cors"
	"github.com/rs/xid"
	"gitlab.com/pdstuber/isit-a-cat-bff/imageretrieval"
	"gitlab.com/pdstuber/isit-a-cat-bff/imageupload"
	"gitlab.com/pdstuber/isit-a-cat-bff/metrics"
	"gitlab.com/pdstuber/isit-a-cat-bff/prediction"
	"gitlab.com/pdstuber/isit-a-cat-bff/prediction/predictionservice"
	"gitlab.com/pdstuber/isit-a-cat-bff/storage"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"gitlab.com/pdstuber/isit-a-cat-bff/healthcheck"
)

const timeoutDuration = time.Millisecond * 1600

var (
	getImageHandler              http.Handler
	postImageHandler             http.Handler
	getPredictionHandler         http.Handler
	metricsMiddleware            *metrics.Middleware
	corsAllowedOrigin            string
	hostPort                     string
	messagingSubjectName         string
	messagingBrokerURL           string
	storageBucketName            string
	storageObjectFolder          string
	objectStorageEndpoint        string
	objectStorageAccessKeyID     string
	objectStorageSecretAccessKey string
	objectStorageUseTLS          bool
	nc                           *nats.Conn
)

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

type idGeneratorImpl struct{}

func (g *idGeneratorImpl) GenerateID() string {
	return xid.New().String()
}

func init() {
	corsAllowedOrigin = getEnv("CORS_ALLOWED_ORIGIN", "*")

	hostPort = getEnv("SERVICE_HOST_PORT", "0.0.0.0:8080")

	objectStorageEndpoint = getEnv("OBJECT_STORAGE_ENDPOINT", "")
	objectStorageAccessKeyID = getEnv("MINIO_ACCESS_KEY", "")
	objectStorageSecretAccessKey = getEnv("MINIO_SECRET_KEY", "")
	objectStorageUseTLS, _ = strconv.ParseBool(getEnv("OBJECT_STORAGE_USE_TLS", "false"))

	storageBucketName = getEnv("STORAGE_BUCKET_NAME", "isit-a-cat")
	storageObjectFolder = getEnv("STORAGE_OBJECT_FOLDER", "uploaded-images/")

	messagingSubjectName = getEnv("MESSAGING_SUBJECT_NAME", "predictions")
	messagingBrokerURL = getEnv("MESSAGING_BROKER_URL", "0.0.0.0:4222")

	storageService, err := storage.New(storageBucketName, storageObjectFolder, objectStorageEndpoint, objectStorageAccessKeyID, objectStorageSecretAccessKey, objectStorageUseTLS)

	if err != nil {
		log.Fatalf("could not create storage service: %v\n", err)
	}

	nc, err = nats.Connect(messagingBrokerURL)

	if err != nil {
		log.Fatalf("Error in creating NATS connection: %v\n", err)
	}

	predictionService := predictionservice.NewService(nc, messagingSubjectName)

	getImageHandler = imageretrieval.NewHandler(storageService)

	postImageHandler = imageupload.NewHandler(storageService, &idGeneratorImpl{})

	// only use for tests
	allowAllOriginChecker := func(r *http.Request) bool { return true }
	getPredictionHandler = prediction.NewHandler(predictionService, &websocket.Upgrader{CheckOrigin: allowAllOriginChecker})
	metricsMiddleware = metrics.NewMiddleware()
}

func createHTTPServer() *http.Server {
	r := mux.NewRouter()
	r.Handle("/images", metricsMiddleware.Decorate("postImageHandler", postImageHandler.ServeHTTP)).Methods("POST")
	r.Handle("/predictions/{id}", metricsMiddleware.Decorate("getPredictionHandler", getPredictionHandler.ServeHTTP)).Methods("GET")
	r.Handle("/images/{id}", metricsMiddleware.Decorate("getImageHandler", getImageHandler.ServeHTTP)).Methods("GET")
	r.HandleFunc("/ping", healthcheck.HandleHealth)
	r.Handle("/metrics", promhttp.Handler())

	c := cors.New(cors.Options{
		AllowedOrigins: []string{corsAllowedOrigin},
		AllowedMethods: []string{"GET", "HEAD", "POST", "PUT", "OPTIONS"},
		AllowedHeaders: []string{"*"},
		Debug:          true,
	})

	h := c.Handler(r)

	return &http.Server{
		Addr:         hostPort,
		WriteTimeout: timeoutDuration,
		ReadTimeout:  timeoutDuration,
		IdleTimeout:  timeoutDuration,
		Handler:      h,
	}

}

func blockAndWaitForSignal(srv *http.Server, nc *nats.Conn) {
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	ctx, cancel := context.WithTimeout(context.Background(), 2)
	defer cancel()
	log.Println("shutting down...")
	err := srv.Shutdown(ctx)

	if err != nil {
		log.Printf("could not shut down http server gracefully: %v\n", err)
	}
	err = nc.Drain()
	if err != nil {
		log.Printf("could not shut down nats connection gracefully: %v\n", err)
	}
	os.Exit(0)
}

func main() {

	srv := createHTTPServer()

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Println(err)
		}
	}()

	blockAndWaitForSignal(srv, nc)
}
