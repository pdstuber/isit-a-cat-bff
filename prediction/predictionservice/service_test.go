package predictionservice

import (
	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	"github.com/vmihailenco/msgpack/v5"
	"gitlab.com/pdstuber/isit-a-cat-bff/prediction/predictionservice/mocks"
	"strings"
	"testing"
)

const (
	testMessagingSubject         = "abc"
	mockSubject                  = "xyz"
	mockReplyTo                  = "uuu"
	mockImageID                  = "1337"
	mockImageClass               = "cat"
	mockImageProbability float32 = 0.99
	mockErrorMessage             = "everything went to hell"
)

type mockError struct{}

func (e mockError) Error() string {
	return mockErrorMessage
}

var (
	mockPrediction = Prediction{
		Class:       mockImageClass,
		Probability: mockImageProbability,
	}
	mockNatsData, _ = msgpack.Marshal(&mockPrediction)
	mockNatsMessage = nats.Msg{
		Subject: mockSubject,
		Reply:   mockReplyTo,
		Data:    mockNatsData,
	}
	mockErrorNatsMessage = nats.Msg{
		Subject: mockSubject,
		Reply:   mockReplyTo,
		Data:    []byte{1, 2, 3},
	}
)

func TestService_GetPredictionForImageId_good_case(t *testing.T) {
	messagingConnectorMock := new(mocks.MessagingConnector)
	messagingConnectorMock.On("Request", mock.Anything, mock.Anything, mock.Anything).Return(&mockNatsMessage, nil)
	predictionService := NewService(messagingConnectorMock, testMessagingSubject)

	prediction, err := predictionService.GetPredictionForImageID(mockImageID)

	messagingConnectorMock.AssertNumberOfCalls(t, "Request", 1)

	assert.NoError(t, err)
	assert.Equal(t, mockImageClass, prediction.Class)
	assert.Equal(t, mockImageProbability, prediction.Probability)
}

func TestService_GetPredictionForImageId_error_msgpack_unmarshall_response(t *testing.T) {
	messagingConnectorMock := new(mocks.MessagingConnector)
	messagingConnectorMock.On("Request", mock.Anything, mock.Anything, mock.Anything).Return(&mockErrorNatsMessage, nil)
	predictionService := NewService(messagingConnectorMock, testMessagingSubject)

	_, err := predictionService.GetPredictionForImageID(mockImageID)

	messagingConnectorMock.AssertNumberOfCalls(t, "Request", 1)
	assert.Error(t, err)
	assert.Condition(t, func() (success bool) {
		return strings.Contains(err.Error(), errorTextCouldNotUnmarshalPredictionResponse)
	})
}

func TestService_GetPredictionForImageId_error_sending_message(t *testing.T) {
	messagingConnectorMock := new(mocks.MessagingConnector)
	messagingConnectorMock.On("Request", mock.Anything, mock.Anything, mock.Anything).Return(nil, mockError{})
	predictionService := NewService(messagingConnectorMock, testMessagingSubject)

	_, err := predictionService.GetPredictionForImageID(mockImageID)

	messagingConnectorMock.AssertNumberOfCalls(t, "Request", 1)
	assert.Error(t, err)
	assert.Equal(t, mockError{}, errors.Cause(err))
}
