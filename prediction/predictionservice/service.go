package predictionservice

import (
	"github.com/nats-io/nats.go"
	"github.com/pkg/errors"
	"github.com/vmihailenco/msgpack/v5"
	"log"
	"time"
)

const (
	errorTextCouldNotMarshalPredictionInput      = "could not marshal prediction input"
	errorTextCouldNotSendMessage                 = "could not send nats message"
	errorTextCouldNotUnmarshalPredictionResponse = "could not unmarshal prediction response"
	timeoutDuration                              = 1500 * time.Millisecond
)

// MessagingConnector makes requests to the message broker
type MessagingConnector interface {
	Request(subj string, data []byte, timeout time.Duration) (*nats.Msg, error)
}

// The Service retrieves predictions for a given image from the prediction microservice
type Service struct {
	messagingConnector   MessagingConnector
	messagingSubjectName string
}

// Input describes the request message format
type Input struct {
	ID string `json:"id"`
}

// Prediction describes the response message format
type Prediction struct {
	Class       string  `json:"class"`
	Probability float32 `json:"probability"`
}

// NewService creates a new instance of the prediction service
func NewService(messagingConnector MessagingConnector, messagingSubjectName string) *Service {
	return &Service{messagingConnector, messagingSubjectName}
}

// GetPredictionForImageID from prediction microservice via messaging
func (s *Service) GetPredictionForImageID(id string) (*Prediction, error) {
	predictionInput, err := msgpack.Marshal(Input{ID: id})

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotMarshalPredictionInput)
	}

	msg, err := s.messagingConnector.Request(s.messagingSubjectName, predictionInput, timeoutDuration)

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotSendMessage)
	}

	predictionResultBytes := msg.Data
	var predictionResult Prediction

	err = msgpack.Unmarshal(predictionResultBytes, &predictionResult)

	if err != nil {
		return nil, errors.Wrap(err, errorTextCouldNotUnmarshalPredictionResponse)
	}

	log.Printf("Got prediction from service: %v\n", predictionResult)

	return &predictionResult, nil
}
