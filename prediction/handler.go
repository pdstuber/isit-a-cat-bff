package prediction

import (
	"github.com/gorilla/mux"
	"github.com/gorilla/websocket"
	"github.com/pkg/errors"
	"gitlab.com/pdstuber/isit-a-cat-bff/prediction/predictionservice"
	"log"
	"net/http"
)

const (
	errorTypeServerError = "SERVER_ERROR"
	errorTypeClientError = "CLIENT_ERROR"
	errorTextMissingID   = "request is missing mandatory path parameter 'id'"
)

var serverErrorResponse = ErrorResponse{
	ErrorType: errorTypeServerError,
}

// An ImagePredictor predicts properties of the image with the given ID
type ImagePredictor interface {
	GetPredictionForImageID(id string) (*predictionservice.Prediction, error)
}

// GetPredictionHandlerImpl handles http requests for predicting images
type GetPredictionHandlerImpl struct {
	imagePredictor    ImagePredictor
	websocketUpgrader *websocket.Upgrader
}

// Result contains the result of a prediction for an image
type Result struct {
	*predictionservice.Prediction
	error
}

// An ErrorResponse is sent back to the client in case an error occurred
type ErrorResponse struct {
	ErrorType string
	Message   string
}

// NewHandler creates an instance of the prediction handler
func NewHandler(imagePredictor ImagePredictor, websocketUpgrader *websocket.Upgrader) http.Handler {
	return &GetPredictionHandlerImpl{imagePredictor, websocketUpgrader}
}

// ServeHTTP requests on the image prediction endpoint
func (h *GetPredictionHandlerImpl) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	ws, err := h.websocketUpgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Printf("error upgrading from http to websocket: %v\n", err)
		return
	}

	defer ws.Close()

	id := mux.Vars(r)["id"]

	if id == "" {
		log.Println(errorTextMissingID)
		clientErrorResponse := ErrorResponse{
			ErrorType: errorTypeClientError,
			Message:   errorTextMissingID,
		}
		err := ws.WriteJSON(&clientErrorResponse)
		if err != nil {
			log.Printf("error in writing websocket error response: %v\n", err)
		}
		return
	}

	h.triggerPrediction(id, ws)
}

func (h *GetPredictionHandlerImpl) triggerPrediction(id string, ws *websocket.Conn) {
	defer func() {
		err := ws.Close()
		if err != nil {
			log.Printf("Error closing websocket: %v\n", err)
		}
	}()

	predictionResultChannel := make(chan Result)

	go h.getPredictionFromService(id, predictionResultChannel)

	prediction := <-predictionResultChannel

	if prediction.error != nil {
		log.Printf("Error getting predictions: %v\n", prediction.error)
		err := ws.WriteJSON(&serverErrorResponse)
		if err != nil {
			log.Printf("error in writing websocket error response: %v\n", err)
		}
		return
	}
	if err := ws.WriteJSON(prediction.Prediction); err != nil {
		log.Printf("error writing json response to websocket : %v\n", err)

		err := ws.WriteJSON(&serverErrorResponse)
		if err != nil {
			log.Printf("error in writing websocket error response: %v\n", err)
		}
		return
	}
}

func (h GetPredictionHandlerImpl) getPredictionFromService(id string, predictionResultChannel chan Result) {
	imagePrediction, err := h.imagePredictor.GetPredictionForImageID(id)

	if err != nil {
		predictionResultChannel <- Result{nil, errors.Wrap(err, "Error getting prediction from prediction service")}
	}

	predictionResultChannel <- Result{imagePrediction, nil}
}
