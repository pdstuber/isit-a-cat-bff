package imageupload

import (
	"encoding/json"
	"gitlab.com/pdstuber/isit-a-cat-bff/imageretrieval"
	"io/ioutil"
	"log"
	"net/http"
)

const (
	headerNameContentType      = "Content-Type"
	headerValueContentTypeJSON = "application/json"
	fileFormKey                = "file"
	staticPictureName          = "picture.jpg"
	errorTextInvalidForm       = "invalid or missing http form data"
	errorTextInvalidFormFile   = "invalid form key. Please provide an image file under they key 'file'"
)

// A StorageWriter writes data to the storage object with the given ID
type StorageWriter interface {
	WriteToBucketObject(objectID string, data []byte) error
}

// A IDGenerator generates unique ids as string
type IDGenerator interface {
	GenerateID() string
}

// PostImageHandlerImpl handles http requests for uploading images
type PostImageHandlerImpl struct {
	storageWriter StorageWriter
	idGenerator   IDGenerator
}

// NewHandler creates a new http handler for uploading images
func NewHandler(storageWriter StorageWriter, idGenerator IDGenerator) http.Handler {
	return &PostImageHandlerImpl{storageWriter, idGenerator}
}

// ServeHTTP requests on the image upload endpoint
func (h PostImageHandlerImpl) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := r.ParseMultipartForm(32 << 20)
	if err != nil {
		log.Printf("invalid http form: %v\n", err)
		http.Error(w, errorTextInvalidForm, http.StatusBadRequest)
		return
	}
	file, _, err := r.FormFile(fileFormKey)

	if err != nil {
		log.Printf("Could not extract file from HTTP form: %v\n", err)
		http.Error(w, errorTextInvalidFormFile, http.StatusBadRequest)
		return

	}

	defer file.Close()

	id := h.idGenerator.GenerateID()

	data, err := ioutil.ReadAll(file)

	if err != nil {
		log.Printf("Could not read image from HTTP form: %v\n", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}

	err = h.storageWriter.WriteToBucketObject(id, data)

	if err != nil {
		log.Printf("Could not upload image to object storage: %v\n", err)
		http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
		return
	}
	imgParams := imageretrieval.ImgParams{
		ID:           id,
		OriginalName: staticPictureName,
	}

	w.Header().Set(headerNameContentType, headerValueContentTypeJSON)
	w.WriteHeader(http.StatusOK)
	marshalledImgParams, _ := json.Marshal(imgParams)
	_, err = w.Write(marshalledImgParams)

	if err != nil {
		log.Printf("Could not write respnse: %v\n", err)
	}
}
